import requests
import json
from pprint import pprint
import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText

"""
Modify these please
"""
#For NXAPI to authenticate the client using client certificate, set 'client_cert_auth' to True.
#For basic authentication using username & pwd, set 'client_cert_auth' to False.

def send_email(send_message):
    f2 = open("email_password.txt")
    email_password = f2.read()
    f2.close

    content = MIMEMultipart()  #建立MIMEMultipart物件
    content["subject"] = "Cisco Nexus 9K running config"  #郵件標題
    content["from"] = "howard322234@gmail.com"  #寄件者
    content["to"] = "howard322234@gmail.com" #收件者
    content.attach(MIMEText(send_message))  #郵件內容

    with smtplib.SMTP(host="smtp.gmail.com", port="587") as smtp:  # 設定SMTP伺服器
        try:
            smtp.ehlo()  # 驗證SMTP伺服器
            smtp.starttls()  # 建立加密傳輸
            smtp.login("howard322234@gmail.com", "mnasxuqhwuslcnnu")  # 登入寄件者gmail
            smtp.send_message(content)  # 寄送郵件
            print("Complete!")
        except Exception as e:
            print("Error message: ", e)

def get_config():
  switchuser='admin'
  switchpassword='eve'


  url='https://192.168.186.201/ins'
  myheaders={'content-type':'application/json-rpc'}
  payload=[
    {
      "jsonrpc": "2.0",
      "method": "cli",
      "params": {
        "cmd": "show running-config",
        "version": 1
      },
      "id": 1
    }
  ]


  response = requests.post(url,data=json.dumps(payload), headers=myheaders,auth=(switchuser,switchpassword), verify = False).text
  print(response)

  
  
  return response

if __name__ == '__main__':
  config_text = get_config()
  f = open('n9k_config.txt')
  prev_config_text = f.read()
  f.close
  send_message = ""
  if config_text == prev_config_text:
    send_message = "config not changed!"
  
  else:
    send_message = config_text
    with open('n9k_config.txt', 'w') as file:
      file.write(config_text)
  
  send_email(send_message)